$(document).ready(function(){
    window.onscroll = function() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            $("#myBtn").css('display', 'block');
        } else {
            $("#myBtn").css('display', 'none');
        }
    };
    if (getCookie('dark-theme') === 'true'){
        $("#theme").click()
    }
})

$("#theme").change(function () { 
     $("body").toggleClass("body-dark");
     $("nav").toggleClass("navbar-dark-active");
     $(".page-footer").toggleClass("navbar-dark-active");
     $("p").toggleClass("text-dark");
     $("h5").toggleClass("text-dark");
     $(".badge").toggleClass("badge-dark");
     $(".accordion-title").toggleClass("accordion-dark");
     $(".accordion-content").toggleClass("accordion-content-dark");
     $("ul").toggleClass("text-dark");
     $("li").toggleClass("text-dark");
     $("#myBtn").toggleClass("upBtn-dark");
     document.cookie = `dark-theme=${this.checked}`;
});

var topFunction = () => {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}

// framework animation needs 
new WOW().init();



function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
}