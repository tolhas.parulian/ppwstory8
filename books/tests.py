from django.test import TestCase
from django.urls import resolve
from .views import books_index
from django.apps import apps
from .apps import BooksConfig
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.utils import timezone
import datetime
import unittest
import time

class booksUnitTest(TestCase):
    def test_books_url_is_exist(self):
        response = self.client.get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_books_url_is_notexist(self):
        response = self.client.get('/tidakada/')
        self.assertEqual(response.status_code, 404)    

    def test_using_books_index_func(self):
        found = resolve('/books/')
        self.assertEqual(found.func, books_index)

    def test_books_content_contains_greeting(self):
        response = self.client.get('/books/')
        self.assertIn("Find your book here!", response.content.decode('utf-8'))    

    def test_using_books_template(self):
        response = self.client.get('/books/')
        self.assertTemplateUsed(response, "books/books.html")    

    def test_books_apps(self):
        self.assertEqual(BooksConfig.name, 'books')
        self.assertEqual(apps.get_app_config('books').name, 'books')

class booksFunctionalTest(TestCase):
    def setUp(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(chrome_options = chrome_options)
        super(booksFunctionalTest, self).setUp()


    def tearDown(self):
        self.selenium.quit()   
        super(booksFunctionalTest, self).tearDown()

    def test_visit_books(self):
        selenium = self.selenium
        selenium.get("http://localhost:8000/books/")
        time.sleep(2)
        greeting = selenium.find_element_by_id('greeting_books').text
        search_field = selenium.find_element_by_id('search_field')
        search_field.send_keys('Joyful')
        search_field.submit()
        time.sleep(3)
        result = selenium.find_elements_by_class_name('card-img-top')
        self.assertTrue(len(result) > 0)
        self.assertIn("Books", selenium.title)
        self.assertIn("Find your book here!", greeting)
        
